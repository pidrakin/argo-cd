# Pidrakin Argo CD Files

## Premise

* Ingress is Nginx
* Cert-Manager is installed
* Letsencrypt Cluster-Issuer with name **letsencrypt-prod** is available

## Install Argo CD

### Install in Cluster

```bash
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj-labs/applicationset/v0.2.0/manifests/install-with-argo-cd.yaml
```

### Install Argo CD CLI

```bash
brew install argocd
```

### Expose Argo CD UI

```bash
curl -o argocd.ingress.yaml https://gitlab.com/pidrakin/argo-cd/-/raw/main/meta/argocd.ingress.yaml.template
vim argocd.ingress.yaml # replace cluster-issuer and argocd-domain
kubectl apply -n argocd -f argocd.ingress.yaml
```

### Login to Argo CD

```bash
ADMIN_PW=$(kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d)
argocd login <ARGOCD_SERVER>
argocd account update-password \
  --account admin \
  --current-password "$ADMIN_PW"
```

### Register a Cluster in Argo CD

```bash
kubectl config get-contexts -o name
argocd cluster add <CLUSTER>
```

### Add Users and Permission

```bash
curl -o argocd-cm.patch.yaml https://gitlab.com/pidrakin/argo-cd/-/raw/main/meta/argocd-cm.patch.yaml.template
curl -o argocd-rbac-cm.patch.yaml https://gitlab.com/pidrakin/argo-cd/-/raw/main/meta/argocd-rbac-cm.patch.yaml.template
vim argocd-cm.yaml # change username and potentially add more lines for more users
vim argocd-rbac-cm.yaml # change username and potentially add more lines for more users
kubectl patch -n argocd configmap argocd-cm --patch "$(cat argocd-cm.patch.yaml)"
kubectl patch -n argocd configmap argocd-rbac-cm --patch "$(cat argocd-rbac-cm.patch.yaml)"
# for every user
argocd account update-password \
  --account <USERNAME> \
  --current-password "$ADMIN_PW"
```

## Prepare Cluster for Appliction Sets

### Create Duck-Type

```bash
kubectl apply -f https://gitlab.com/pidrakin/argo-cd/-/raw/main/placementrule-crd.yaml
```

### Patch Application Set Controller Role to use Duck-Type

```bash
curl -o argocd-applicationset-controller.role.patch.yaml https://gitlab.com/pidrakin/argo-cd/-/raw/main/argocd-applicationset-controller.role.patch.yaml
kubectl patch -n argocd role argocd-applicationset-controller --patch "$(cat argocd-applicationset-controller.role.patch.yaml)"
```

### Add Cluster Decision Config Map for Generator

```bash
curl -o cluster-decision-cm.yaml https://gitlab.com/pidrakin/argo-cd/-/raw/main/cluster-decision-cm.yaml
kubectl apply -n argocd -f cluster-decision-cm.yaml
```

## Install Application

### Add Repository with Secret

```bash
curl -o repository-secret.yaml https://gitlab.com/pidrakin/argo-cd/-/raw/main/repository-secret.yaml.template
vim repository-secret.yaml # change username and password
kubectl apply -n argocd -f repository-secret.yaml
```

### Add Duck-Type

```bash
curl -o placementrule.yaml https://gitlab.com/pidrakin/argo-cd/-/raw/main/mik3os/placementrule.yaml.template
vim placementrule.yaml # change e.g. api-key
kubectl apply -n argocd -f placementrule.yaml
```

### Install

```bash
kubectl apply -n argocd -f https://gitlab.com/pidrakin/argo-cd/-/raw/main/<APP>/applicationset.yaml
```

